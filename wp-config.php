<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wpl');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'wpl');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5_:05.ZppFXtJR._|u?iChByuRG54V~VQ.d3cmxWA~YK&;&@2uhr#QNORiroTK3M');
define('SECURE_AUTH_KEY',  'AAkaLZU3Ts.JTFDU BW@29j/_rQ~pN($+E[|~lL~^Vq|18].w0$-.v7m~Vb#hG77');
define('LOGGED_IN_KEY',    'tGbXk2TVUI0+&t)%JL(0F-D>e*1ilmfqSvO?R(jU=eq@_mxANvr01^!62w!PrY4%');
define('NONCE_KEY',        'fTHTynnblhWjF3VBNQ`KXcgX&#D36r2z o<.KOH%n*GuF=_OfpK8lq$E7:,3$x@z');
define('AUTH_SALT',        'vBdN3nA>j]+NPfM3rui6vrII;)C><KcZ^WO7H^-6fB&hfpdl~?Hu448Krl~=dfR1');
define('SECURE_AUTH_SALT', 'OrhcL[FTgD#:[G/| E4SK)A5.qG){,/i$<k2t5=39>=^[-iT2H~y}!rTq:o}[4t3');
define('LOGGED_IN_SALT',   'X_pTgjG%*YdS$|n hw3vpW99p%0dzgAfe9^)A`%{|jDug}e|fqHw~aZm#IZ=9Y8_');
define('NONCE_SALT',       '!4ST4lTa)DfY78uF^A 4Wk|VTj9tEzfw_K[6~6-My]1eca#=RC2b]eV;k OhbbgL');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wpl_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
